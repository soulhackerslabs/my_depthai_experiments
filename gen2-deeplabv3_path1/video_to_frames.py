import cv2

import ffmpeg    

def check_rotation(path_video_file):
	# this returns meta-data of the video file in form of a dictionary
	meta_dict = ffmpeg.probe(path_video_file)

	# from the dictionary, meta_dict['streams'][0]['tags']['rotate'] is the key
	# we are looking for
	rotateCode = None


	if 'rotate' not in meta_dict['streams'][0]['tags']:
		return None
	if int(meta_dict['streams'][0]['tags']['rotate']) == 90:
		rotateCode = cv2.ROTATE_90_CLOCKWISE
	elif int(meta_dict['streams'][0]['tags']['rotate']) == 180:
		rotateCode = cv2.ROTATE_180
	elif int(meta_dict['streams'][0]['tags']['rotate']) == 270:
		rotateCode = cv2.ROTATE_90_COUNTERCLOCKWISE

	return rotateCode

def correct_rotation(frame, rotateCode):  
	return cv2.rotate(frame, rotateCode) 

video_path = '/media/carlos/Data/training/videos/PXL_20210520_101835394.LS.mp4'
vidcap = cv2.VideoCapture(video_path)
output_path = "/media/carlos/Data/training/PXL_20210520_101835394"
count = 0
skip = 5

# check if video requires rotation
rotateCode = check_rotation(video_path)
print("Rotate code",rotateCode)

while True:
	success,image = vidcap.read()
	if success:
		
		# check if the frame needs to be rotated
		if rotateCode is not None:
			print("rotate")
			correct_rotation(image, rotateCode)

		if count % skip == 0:
			cv2.imwrite(output_path+"/frame4-%d.jpg" % count, image)     # save frame as JPEG file   
		cv2.imshow("Image", image)
		

		if cv2.waitKey(1) == ord('q'):
			break   
	
		count += 1  

	else:
		break
		
print(count)