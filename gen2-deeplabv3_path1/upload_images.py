import superannotate as sa
import json

TOKEN = "7d01d84a435b721e2940c4ba8cd5f31ba4b6e094b630cbc6230be2f174f568af660eaf12c878873ft=6272"
PROJECT_NAME = "Startup Terrace Red Path"
	
token_json = {"token": TOKEN}

with open('sa_config.json', 'w') as f:
	json.dump(token_json, f)

	sa.init("/home/carlos/.superannotate/config.json")

	sa.create_project("Startup Terrace Red Path", "The Startup Terrace red path by the Sports Park in Linkou", "Pixel")

	sa.upload_images_from_folder_to_project("Startup Terrace Red Path", "/media/carlos/Data/training/fl_oak_03-06-2021-05-59-27-some")

