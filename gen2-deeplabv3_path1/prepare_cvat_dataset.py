
import json
import os 
from shutil import copyfile, make_archive
from os.path import isfile, join
from os import listdir, remove
from PIL import Image
import re

import time

import cv2

dataset_base_dir = "/media/carlos/Data/training/annotated/yellowlights-fixed"
#"/media/carlos/Data/training/annotated/startup-red-path-22-plus-noon-shadows/VOC2012"
segmentation_dir = "SegmentationClass"
images_dir = "JPEGImages"

def fix_bad_names_in_zip():
	import zipfile

	target = zipfile.ZipFile('/media/carlos/Data/training/annotated/yellowlights-fixed.zip', 'w', zipfile.ZIP_DEFLATED)
	source = zipfile.ZipFile('/media/carlos/Data/training/annotated/yellowlights.zip', 'r')

	for file in source.filelist:
		print(file)

		filename = file.filename
		filename = filename.replace(":","-")
		filename = filename.replace(" ","-")

		print(filename)

		target.writestr(filename, source.read(file.filename))

	target.close()
	source.close()

def fix_bad_names_in_folder():
	import zipfile

	source_path = '/home/carlos/Pictures/training-data/3_12-11-2021-09-40-42/color'
	files = [f for f in listdir(source_path) if isfile(join(source_path, f))]
	
	for file in files:
		print(file)

		filename = file.replace(":","-")
		filename = filename.replace(" ","-")

		os.rename(join(source_path, file),join(source_path, filename))
	

def remove_unnanotated_images_pascal():
	import xml.etree.ElementTree as ET
	
	dataset_base_dir = "/media/carlos/Data/training/annotated/205+track1-all-signs"
	annotations_dir = dataset_base_dir + "/Annotations"
	images_dir = dataset_base_dir + "/JPEGImages"
	files = [f for f in listdir(annotations_dir) if isfile(join(annotations_dir, f))]

	for file in files:
		tree = ET.parse(join(annotations_dir, file))
		root = tree.getroot()
		#print(file)
		annotated = False
		for child in root:
			#print(child.tag)
			if child.tag == "object":
				annotated = True
				#print("Annotated")
				break
		if not annotated:
			print(file)
			base_file = file.split(".xml")[0]
			os.remove(join(images_dir, base_file+".jpg"))
			os.remove(join(annotations_dir, file))


def change_filename_pascal():
	import xml.etree.ElementTree as ET
	
	dataset_base_dir = "/media/carlos/Data/training/annotated/205+track1-all-signs/sorted"
	folder_dir = dataset_base_dir + "/validation"
	files = [f for f in listdir(folder_dir) if isfile(join(folder_dir, f)) and ".xml" in f]

	for file in files:
		tree = ET.parse(join(folder_dir, file))
		root = tree.getroot()
		#print(file)
		
		elems = tree.findall('filename')

		for elem in elems:
			
			new_file = file.replace(".xml",".jpg")
			new_file = new_file.replace(".png",".jpg")
			elem.text = new_file
			print(elem.text,file)
		tree.write(join(folder_dir, file))

def remove_unnanotated_images():
	segmentation_dir_full = join(dataset_base_dir, segmentation_dir)
	segmentation_files = [f for f in listdir(segmentation_dir_full) if isfile(join(segmentation_dir_full, f))]
	

	images_dir_full = join(dataset_base_dir, images_dir)
	images_files = [f for f in listdir(images_dir_full) if isfile(join(images_dir_full, f))]

	for f in images_files:
		if f not in segmentation_files:
			os.remove(join(images_dir_full, f))

	'''
	for f in segmentation_files:
		frame = cv2.imread(join(images_dir_full, f), 1) 
		segmentation = cv2.imread(join(segmentation_dir_full, f), 1)
		frame = cv2.addWeighted(frame,1, segmentation,0.8,0)

		cv2.imshow("Image", frame)

		cv2.waitKey(0)
	'''
		



def convert_images():
	images_dir_full = join(dataset_base_dir, images_dir)
	images_files = [f for f in listdir(images_dir_full) if isfile(join(images_dir_full, f))]
	#print(files)
	for file in images_files:
		file_path = join(images_dir_full, file)
		
		im1 = Image.open(file_path)
		file_path_to_save = file_path.split(".png")[0]
		im1.save(file_path_to_save+'.jpg')

		os.remove(file_path)


def show_segmentation():
	segmentation_dir_full = join(dataset_base_dir, segmentation_dir)
	segmentation_files = [f for f in listdir(segmentation_dir_full) if isfile(join(segmentation_dir_full, f))]
	

	images_dir_full = join(dataset_base_dir, images_dir)
	images_files = [f for f in listdir(images_dir_full) if isfile(join(images_dir_full, f))]

	
	for i, f in enumerate(segmentation_files):
		print(f)
		image_f = f.split(".png")[0]+".jpg"
		frame = cv2.imread(join(images_dir_full, image_f), 1) 
		segmentation = cv2.imread(join(segmentation_dir_full, f), 1)
		print(frame.shape)
		print(segmentation.shape)
		frame = cv2.addWeighted(frame,1, segmentation,0.8,0)

		cv2.imshow("Image", frame)

		cv2.waitKey(0)

		#if i == 20:
		#	break
	

def change_file_name():
	files = [join(dataset_base_dir,f) for f in listdir(dataset_base_dir) if isfile(join(dataset_base_dir, f)) and 'jpg' not in f]
	for file_path in files:
		file_path_to_save = file_path.replace(".png___", ".jpg___")
		os.rename(file_path, file_path_to_save)


def split_dataset_pascal():
	import numpy as np
	from sklearn.model_selection import train_test_split


	dataset_base_dir = "/media/carlos/Data/training/annotated/205+track1-all-signs"
	annotations_dir = dataset_base_dir + "/Annotations"
	images_dir = dataset_base_dir + "/JPEGImages"
	
	files = [f for f in listdir(annotations_dir) if isfile(join(annotations_dir, f))]

	X = []
	y = []
	for file in files:
		
		base_file = file.split(".xml")[0]
		jpg_file = base_file+".jpg"

		y.append(file)
		X.append(jpg_file)

	print("X: ",X[:10],"\ny: ",y[:10])

	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

	print("X_train: ",X_train[:10],"\nX_test: ",X_test[:10],"\ny_train: ",y_train[:10],"\ny_test: ",y_test[:10])

	sorted_path = os.path.join(dataset_base_dir, "sorted")
	train_path = os.path.join(sorted_path, "train")
	valid_path = os.path.join(sorted_path, "validation")
	test_path = os.path.join(sorted_path, "test")
	
	if not os.path.exists(sorted_path):
		os.mkdir(sorted_path)
		os.mkdir(train_path)
		os.mkdir(valid_path)
		os.mkdir(test_path)

	for file in X_train:
		copyfile(join(images_dir, file), join(train_path, file))

	for file in y_train:
		copyfile(join(annotations_dir, file), join(train_path, file))


	for file in X_test:
		copyfile(join(images_dir, file), join(valid_path, file))

	for file in y_test:
		copyfile(join(annotations_dir, file), join(valid_path, file))








#remove_unnanotated_images()
#convert_images()
#show_segmentation()

#fix_bad_names_in_zip()
#remove_unnanotated_images_pascal()
#split_dataset_pascal()
#fix_bad_names_in_folder()
change_filename_pascal()