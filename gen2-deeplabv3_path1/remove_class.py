import json
import os 
from shutil import copy, make_archive
from os.path import isfile, join
from os import listdir, remove
from PIL import Image
import re

classes = ["Path"]
skip_files = {}

dataset_base_dir = "/media/carlos/Data/training/annotated/path1"

# Read all jsons and determine files to keep
files = [f for f in listdir(dataset_base_dir) if isfile(join(dataset_base_dir, f)) and ".json" in f ]

for file_name  in files:
	file_path = join(dataset_base_dir, file_name)
	data = None
	#print(file_path)
	with open(file_path) as f:
		content = f.read()
		#print(content)
		data = json.loads(content)
		for instance in data["instances"]:
			if instance["className"] not in classes:
				base_name = file_name.split("___pixel.json")[0]
				skip_files[base_name] = base_name

#print(skip_files)

files = [f for f in listdir(dataset_base_dir) if isfile(join(dataset_base_dir, f)) ]
for file_name in files:
	for skip_file_name in skip_files:
		if skip_file_name in file_name:
			file_path = join(dataset_base_dir, file_name)
			print("remove ",file_path)
			os.remove(file_path)
			continue

			
		