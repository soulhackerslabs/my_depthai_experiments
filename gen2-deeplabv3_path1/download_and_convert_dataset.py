import superannotate as sa
import json
import os 
from shutil import copy, make_archive
from os.path import isfile, join
from os import listdir, remove
from PIL import Image
import re

dataset_base_dir = "/media/carlos/Data/training/annotated/path1"

def download_data():
	TOKEN = "7d01d84a435b721e2940c4ba8cd5f31ba4b6e094b630cbc6230be2f174f568af660eaf12c878873ft=6272"
	PROJECT_NAME = "Linkou Sports Park Path"
	
	token_json = {"token": TOKEN}

	with open('sa_config.json', 'w') as f:
	  json.dump(token_json, f)

	sa.init('sa_config.json')

	export = sa.prepare_export(PROJECT_NAME, annotation_statuses=["Completed"], include_fuse=True)
	sa.download_export(PROJECT_NAME, export, dataset_base_dir)


def convert_images():
	files = [f for f in listdir(dataset_base_dir) if isfile(join(dataset_base_dir, f)) and "__fuse" not in f and "__save"  not in f  ]
	#print(files)
	for file in files:
		file_path = join(dataset_base_dir, file)
		if "png" in file and ".json" not in file:
			im1 = Image.open(file_path)
			file_path_to_save = file_path.split(".png")[0]
			im1.save(file_path_to_save+'.jpg')

			os.remove(file_path)

		if "json" in file:
			file_path = join(dataset_base_dir, file)
			data = None
			#print(file_path)
			with open(file_path) as f:
				content = f.read()
				#print(content)
				data = json.loads(content)
				data["metadata"]["name"] = data["metadata"]["name"].split(".png")[0]+'.jpg'
			
			with open(file_path, 'w') as f:
				json.dump(data,f)

def change_file_name():
	files = [join(dataset_base_dir,f) for f in listdir(dataset_base_dir) if isfile(join(dataset_base_dir, f)) and 'jpg' not in f]
	for file_path in files:
		file_path_to_save = file_path.replace(".png___", ".jpg___")
		os.rename(file_path, file_path_to_save)

#download_data()
convert_images()
change_file_name()