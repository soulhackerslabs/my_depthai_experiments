#!/usr/bin/env python3

import cv2
from datetime import datetime
from pathlib import Path
import depthai as dai
import numpy as np
import argparse

def check_range(min_val, max_val):
    def check_fn(value):
        ivalue = int(value)
        if min_val <= ivalue <= max_val:
            return ivalue
        else:
            raise argparse.ArgumentTypeError(
                "{} is an invalid int value, must be in range {}..{}".format(value, min_val, max_val)
            )
    return check_fn

cam_options = ["14442C109130A1D000","14442C10C1FAA1D000","14442C10C1CF0FD100", "14442C100199A1D000"]


parser = argparse.ArgumentParser()
parser.add_argument('-t', '--threshold', default=0.3, type=float, help="Maximum difference between packet timestamps to be considered as synced")
parser.add_argument('-p', '--path', default=None, type=str, help="Path where to store the captured data")
parser.add_argument('-d', '--dirty', action='store_true', default=False, help="Allow the destination path not to be empty")
parser.add_argument('-nd', '--no-debug', dest="prod", action='store_true', default=False, help="Do not display debug output")
parser.add_argument('-m', '--time', type=float, default=float("inf"), help="Finish execution after X seconds")
parser.add_argument('-af', '--autofocus', type=str, default=None, help="Set AutoFocus mode of the RGB camera", choices=list(filter(lambda name: name[0].isupper(), vars(dai.CameraControl.AutoFocusMode))))
parser.add_argument('-mf', '--manualfocus', type=check_range(0, 255), help="Set manual focus of the RGB camera [0..255]")
parser.add_argument('-et', '--exposure-time', type=check_range(1, 33000), help="Set manual exposure time of the RGB camera [1..33000]")
parser.add_argument('-ei', '--exposure-iso', type=check_range(100, 1600), help="Set manual exposure ISO of the RGB camera [100..1600]")
parser.add_argument("-ci", "--cam_id", help="provide the ID of the camera to use", default='', choices=cam_options)

args = parser.parse_args()

cam_id = args.cam_id

found, device_info = None, None
if cam_id != '':
    found, device_info = dai.Device.getDeviceByMxId(cam_id)

    if not found:
        raise RuntimeError("Device not found!")
else:
    print("No camera ID specified, finding one")
    for device in dai.Device.getAllAvailableDevices():
        print(f"{device.getMxId()} {device.state}")
        cam_id = device.getMxId()
    if cam_id != '':
        print("Using camera ",cam_id)
        found, device_info = dai.Device.getDeviceByMxId(cam_id)
    else:
        raise RuntimeError("No device found!")

def create_paths():
    frame_paths = {}

    frames_path = dest / Path("left")
    frames_path.mkdir(parents=False, exist_ok=False)
    frame_paths["left"] = frames_path

    frames_path = dest / Path("right")
    frames_path.mkdir(parents=False, exist_ok=False)
    frame_paths["right"] = frames_path

    frames_path = dest / Path("disparity")
    frames_path.mkdir(parents=False, exist_ok=False)
    frame_paths["disparity"] = frames_path

    frames_path = dest / Path("color")
    frames_path.mkdir(parents=False, exist_ok=False)
    frame_paths["color"] = frames_path

    return frame_paths

# datetime object containing current date and time
now = datetime.now()
 
# dd/mm/YY H:M:S
dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")

if args.path is None:
    path = cam_id + "_" +dt_string
else:
    path = args.path + "_" +dt_string 

dest = Path(path).resolve().absolute()
dest_count = len(list(dest.glob('*')))
if dest.exists() and dest_count != 0 and not args.dirty:
    raise ValueError(f"Path {dest} contains {dest_count} files. Either specify new path or use \"--dirty\" flag to use current one")
dest.mkdir(parents=True, exist_ok=True)

frame_paths = create_paths()

'''
If one or more of the additional depth modes (lrcheck, extended, subpixel)
are enabled, then:
 - depth output is FP16. TODO enable U16.
 - median filtering is disabled on device. TODO enable.
 - with subpixel, either depth or disparity has valid data.
Otherwise, depth output is U16 (mm) and median is functional.
But like on Gen1, either depth or disparity has valid data. TODO enable both.
'''

# Closer-in minimum depth, disparity range is doubled (from 95 to 190):
extended_disparity = True
# Better accuracy for longer distance, fractional disparity 32-levels:
subpixel = False
# Better handling for occlusions:
lr_check = True

# Start defining a pipeline
pipeline = dai.Pipeline()

# Define a source - two mono (grayscale) cameras
left = pipeline.createMonoCamera()
left.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
left.setBoardSocket(dai.CameraBoardSocket.LEFT)

right = pipeline.createMonoCamera()
right.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
right.setBoardSocket(dai.CameraBoardSocket.RIGHT)

# Create a node that will produce the depth map (using disparity output as it's easier to visualize depth this way)
depth = pipeline.createStereoDepth()
depth.setConfidenceThreshold(200)
depth.setOutputDepth(False)
# Options: MEDIAN_OFF, KERNEL_3x3, KERNEL_5x5, KERNEL_7x7 (default)
median = dai.StereoDepthProperties.MedianFilter.KERNEL_7x7 # For depth filtering
depth.setMedianFilter(median)

depth.setLeftRightCheck(lr_check)

# Normal disparity values range from 0..95, will be used for normalization
max_disparity = 95

if extended_disparity: max_disparity *= 2 # Double the range
depth.setExtendedDisparity(extended_disparity)

if subpixel: max_disparity *= 32 # 5 fractional bits, x32
depth.setSubpixel(subpixel)

# When we get disparity to the host, we will multiply all values with the multiplier
# for better visualization
multiplier = 255 / max_disparity

left.out.link(depth.left)
right.out.link(depth.right)

# Create output
xout = pipeline.createXLinkOut()
xout.setStreamName("disparity")
depth.disparity.link(xout.input)

# Pipeline is defined, now we can connect to the device
with dai.Device(pipeline, device_info) as device:

    # Start pipeline
    device.startPipeline()

    # Output queue will be used to get the disparity frames from the outputs defined above
    q = device.getOutputQueue(name="disparity", maxSize=4, blocking=False)
    while True:
        inDepth = q.get()  # blocking call, will wait until a new data has arrived
        frame = inDepth.getFrame()
        frame = (frame*multiplier).astype(np.uint8)
        # Available color maps: https://docs.opencv.org/3.4/d3/d50/group__imgproc__colormap.html
        frame = cv2.applyColorMap(frame, cv2.COLORMAP_JET)

        # frame is ready to be shown
        cv2.imshow("disparity", frame)
        frames_path = frame_paths["disparity"]
        now = datetime.now()
        dt_string = now.strftime('%Y-%m-%d %H:%M:%S.%f')
        cv2.imwrite(str(frames_path / Path(f"disparity_{dt_string}.png")), frame)

        if cv2.waitKey(1) == ord('q'):
            break
