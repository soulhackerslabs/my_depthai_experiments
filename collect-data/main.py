#!/usr/bin/env python3
import argparse
from pathlib import Path
from time import monotonic
from uuid import uuid4
from multiprocessing import Process, Queue
import cv2
import depthai as dai

from datetime import datetime

def check_range(min_val, max_val):
    def check_fn(value):
        ivalue = int(value)
        if min_val <= ivalue <= max_val:
            return ivalue
        else:
            raise argparse.ArgumentTypeError(
                "{} is an invalid int value, must be in range {}..{}".format(value, min_val, max_val)
            )
    return check_fn


'''
  "14442C109130A1D000" - 1 or blue
  "14442C10C1FAA1D000" - 2 or green
  "14442C10C1CF0FD100" - 3 or yellow
  "14442C100199A1D000" - 4 or orange

'''
cam_options = ["1", "2", "3", "4"]
res_options = ["1080", "720", "480", "360"]
res_dimentions = {"1080":(1920,1080), "720":(1280,720), "480":(854,480), "360":(640,360)}
cam_ids = {"1":"14442C109130A1D000", "2":"14442C10C1FAA1D000", "3":"14442C10C1CF0FD100", "4":"14442C100199A1D000"}


parser = argparse.ArgumentParser()
parser.add_argument('-t', '--threshold', default=0.3, type=float, help="Maximum difference between packet timestamps to be considered as synced")
parser.add_argument('-p', '--path', default=None, type=str, help="Path where to store the captured data")
parser.add_argument('-d', '--dirty', action='store_true', default=False, help="Allow the destination path not to be empty")
parser.add_argument('-nd', '--no-debug', dest="prod", action='store_true', default=False, help="Do not display debug output")
parser.add_argument('-m', '--time', type=float, default=float("inf"), help="Finish execution after X seconds")
parser.add_argument('-af', '--autofocus', type=str, default=None, help="Set AutoFocus mode of the RGB camera", choices=list(filter(lambda name: name[0].isupper(), vars(dai.CameraControl.AutoFocusMode))))
parser.add_argument('-mf', '--manualfocus', type=check_range(0, 255), help="Set manual focus of the RGB camera [0..255]")
parser.add_argument('-et', '--exposure-time', type=check_range(1, 33000), help="Set manual exposure time of the RGB camera [1..33000]")
parser.add_argument('-ei', '--exposure-iso', type=check_range(100, 1600), help="Set manual exposure ISO of the RGB camera [100..1600]")
parser.add_argument("-cn", "--cam_number", help="provide the number of the camera to use", required=False, choices=cam_options)
parser.add_argument("-r", "--resolution", help="RGB resolution", default='720', choices=res_options)

args = parser.parse_args()

cam_id = ''
cam_number = args.cam_number
print(cam_number)
if cam_number is not None:
    cam_id = cam_ids[cam_number]
resolution = args.resolution
manip_resolution = 416
width, height = res_dimentions[resolution]

found, device_info = None, None
if cam_id != '':
	found, device_info = dai.Device.getDeviceByMxId(cam_id)

	if not found:
		raise RuntimeError("Device not found!")
else:
	print("No camera ID specified, finding one")
	for device in dai.Device.getAllAvailableDevices():
		print(f"{device.getMxId()} {device.state}")
		cam_id = device.getMxId()
	if cam_id != '':
		print("Using camera ",cam_id)
		found, device_info = dai.Device.getDeviceByMxId(cam_id)
	else:
		raise RuntimeError("No device found!")

exposure = [args.exposure_time, args.exposure_iso]
if any(exposure) and not all(exposure):
    raise RuntimeError("Both --exposure-time and --exposure-iso needs to be provided")

# datetime object containing current date and time
now = datetime.now()
 
# dd/mm/YY H:M:S
dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")

if args.path is None:
	path = "/media/carlos/Data/training/"+cam_number + "_" +dt_string
else:
	path = args.path + "_" +dt_string 

dest = Path(path).resolve().absolute()
dest_count = len(list(dest.glob('*')))
if dest.exists() and dest_count != 0 and not args.dirty:
    raise ValueError(f"Path {dest} contains {dest_count} files. Either specify new path or use \"--dirty\" flag to use current one")
dest.mkdir(parents=True, exist_ok=True)

pipeline = dai.Pipeline()

rgb = pipeline.createColorCamera()
rgb.setPreviewSize(width, height)
rgb.setBoardSocket(dai.CameraBoardSocket.RGB)
rgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
rgb.setInterleaved(False)
rgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.RGB)

controlIn = pipeline.createXLinkIn()
controlIn.setStreamName('control')
controlIn.out.link(rgb.inputControl)

# Create manip to resize full frame
manip = pipeline.createImageManip()
manip.setResize(manip_resolution, manip_resolution)
manip.setKeepAspectRatio(True)
#manip.setFrameType(dai.RawImgFrame.Type.BGR888p)
rgb.preview.link(manip.inputImage)
   
# Create output
rgbOutFull = pipeline.createXLinkOut()
rgbOutFull.setStreamName("color-full")
#rgb.video.link(rgbOutFull.input)
rgb.preview.link(rgbOutFull.input)

rgbOut = pipeline.createXLinkOut()
rgbOut.setStreamName("color")
manip.out.link(rgbOut.input)


def create_paths():
    frame_paths = {}

    frames_path = dest / Path("color")
    frames_path.mkdir(parents=False, exist_ok=False)
    frame_paths["color"] = frames_path

    frames_path = dest / Path("color-full")
    frames_path.mkdir(parents=False, exist_ok=False)
    frame_paths["color-full"] = frames_path

    return frame_paths

frame_paths = create_paths()

# Pipeline defined, now the device is connected to
#with dai.Device(pipeline) as device:
with dai.Device(pipeline, device_info) as device:
    # Start pipeline
    device.startPipeline()

    qControl = device.getInputQueue('control')

    ctrl = dai.CameraControl()
    if args.autofocus:
        ctrl.setAutoFocusMode(getattr(dai.CameraControl.AutoFocusMode, args.autofocus))
    if args.manualfocus:
        ctrl.setManualFocus(args.manualfocus)
    if all(exposure):
        ctrl.setManualExposure(*exposure)

    qControl.send(ctrl)

    colorQ = device.getOutputQueue("color", 4, False)
    colorFullQ = device.getOutputQueue("color-full", 4, False)
    while True:
        imgFrame = colorQ.get()
        fullImgFrame = colorFullQ.get()
 
        frame = imgFrame.getCvFrame()
        fullFrame = fullImgFrame.getCvFrame()

        now = datetime.now()
        dt_string = now.strftime('%Y-%m-%d-%H-%M-%S-%f')
        #ms = (now.day * 24 * 60 * 60 + now.seconds) * 1000 + now.microseconds / 1000.0
        if frame is not None:
            frames_path = frame_paths['color']
            stream_name = 'color'
            cv2.imwrite(str(frames_path / Path(f"{stream_name}_{dt_string}.jpg")), frame)
            cv2.imshow(cam_number+ "-" +stream_name, frame)

        if fullFrame is not None:
            frames_path = frame_paths['color-full']
            stream_name = 'color-full'
            cv2.imwrite(str(frames_path / Path(f"{stream_name}_{dt_string}.jpg")), fullFrame)
            cv2.imshow(cam_number+ "-" +stream_name, fullFrame)

        if cv2.waitKey(1) == ord('q'):
            break
        
