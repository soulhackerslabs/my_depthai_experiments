#!/usr/bin/env python3

from pathlib import Path
import sys
import cv2
import depthai as dai
import numpy as np
import argparse
import imutils
import time

'''
Spatial Tiny-yolo example
  Performs inference on RGB camera and retrieves spatial location coordinates: x,y,z relative to the center of depth map.
  Can be used for tiny-yolo-v3 or tiny-yolo-v4 networks
'''

cam_options = ["14442C109130A1D000","14442C10C1FAA1D000","14442C10C1CF0FD100", "14442C100199A1D000"]

parser = argparse.ArgumentParser()
parser.add_argument("-ci", "--cam_id", help="provide the ID of the camera to use", default='', choices=cam_options)

args = parser.parse_args()

cam_id = args.cam_id

# tiny yolo v3/4 label texts
labelMap = [
	"person",         "bicycle",    "car",           "motorbike",     "aeroplane",   "bus",           "train",
	"truck",          "boat",       "traffic light", "fire hydrant",  "stop sign",   "parking meter", "bench",
	"bird",           "cat",        "dog",           "horse",         "sheep",       "cow",           "elephant",
	"bear",           "zebra",      "giraffe",       "backpack",      "umbrella",    "handbag",       "tie",
	"suitcase",       "frisbee",    "skis",          "snowboard",     "sports ball", "kite",          "baseball bat",
	"baseball glove", "skateboard", "surfboard",     "tennis racket", "bottle",      "wine glass",    "cup",
	"fork",           "knife",      "spoon",         "bowl",          "banana",      "apple",         "sandwich",
	"orange",         "broccoli",   "carrot",        "hot dog",       "pizza",       "donut",         "cake",
	"chair",          "sofa",       "pottedplant",   "bed",           "diningtable", "toilet",        "tvmonitor",
	"laptop",         "mouse",      "remote",        "keyboard",      "cell phone",  "microwave",     "oven",
	"toaster",        "sink",       "refrigerator",  "book",          "clock",       "vase",          "scissors",
	"teddy bear",     "hair drier", "toothbrush"
]

syncNN = False
segmentationNNShape = 256

def decode_deeplabv3p(output_tensor):
	class_colors = [[0,0,0],  [0,255,0]]
	class_colors = np.asarray(class_colors, dtype=np.uint8)
	
	output = output_tensor.reshape(segmentationNNShape,segmentationNNShape)
	output_colors = np.take(class_colors, output, axis=0)
	return output_colors

def show_deeplabv3p(output_colors, frame):
	output_colors = imutils.resize(output_colors, width=416)
	#print(frame.shape,output_colors.shape)
	return cv2.addWeighted(frame,1, output_colors,0.2,0)

# Get argument first
nnBlobPath = str((Path(__file__).parent / Path('models/tiny-yolo-v4_openvino_2021.2_6shave.blob')).resolve().absolute())
#if len(sys.argv) > 1:
#	nnBlobPath = sys.argv[1]

# Start defining a pipeline
pipeline = dai.Pipeline()

pipeline.setOpenVINOVersion(version = dai.OpenVINO.Version.VERSION_2021_2)

# Define a source - color camera
colorCam = pipeline.createColorCamera()
spatialDetectionNetwork = pipeline.createYoloSpatialDetectionNetwork()
monoLeft = pipeline.createMonoCamera()
monoRight = pipeline.createMonoCamera()
stereo = pipeline.createStereoDepth()
manip = pipeline.createImageManip()
manip.setResize(segmentationNNShape,segmentationNNShape)
manip.setKeepAspectRatio(True)
manip.setFrameType(dai.RawImgFrame.Type.BGR888p)

# Define a neural network that will make segmentation predictions based on the source frames
segmentation_nn = pipeline.createNeuralNetwork()
segmentation_nn.setBlobPath('models/deeplab_v3_plus_mvn2_decoder_256_openvino_2021.2_6shave.blob')
segmentation_nn.setNumPoolFrames(4)
segmentation_nn.setNumInferenceThreads(2)

xoutRgb = pipeline.createXLinkOut()
xoutNN = pipeline.createXLinkOut()
xoutSegm = pipeline.createXLinkOut()
xoutBoundingBoxDepthMapping = pipeline.createXLinkOut()
#xoutDepth = pipeline.createXLinkOut()

xoutRgb.setStreamName("rgb")
xoutNN.setStreamName("detections")
xoutSegm.setStreamName("segmentation")
xoutBoundingBoxDepthMapping.setStreamName("boundingBoxDepthMapping")
#xoutDepth.setStreamName("depth")

# Set all inputs to non-blocking
manip.inputImage.setBlocking(False)
segmentation_nn.input.setBlocking(False)
stereo.left.setBlocking(False)
stereo.right.setBlocking(False)
spatialDetectionNetwork.input.setBlocking(False)
spatialDetectionNetwork.inputDepth.setBlocking(False)
xoutRgb.input.setBlocking(False)
xoutNN.input.setBlocking(False)
xoutSegm.input.setBlocking(False)
xoutBoundingBoxDepthMapping.input.setBlocking(False)


colorCam.setPreviewSize(416, 416)
colorCam.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
colorCam.setInterleaved(False)
colorCam.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)
colorCam.setFps(40)

monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

# setting node configs
stereo.setOutputDepth(True)
stereo.setConfidenceThreshold(255)

spatialDetectionNetwork.setBlobPath(nnBlobPath)
spatialDetectionNetwork.setConfidenceThreshold(0.5)
spatialDetectionNetwork.setBoundingBoxScaleFactor(0.5)
spatialDetectionNetwork.setDepthLowerThreshold(100)
spatialDetectionNetwork.setDepthUpperThreshold(5000)
# yolo specific parameters
spatialDetectionNetwork.setNumClasses(80)
spatialDetectionNetwork.setCoordinateSize(4)
spatialDetectionNetwork.setAnchors(np.array([10,14, 23,27, 37,58, 81,82, 135,169, 344,319]))
spatialDetectionNetwork.setAnchorMasks({ "side26": np.array([1,2,3]), "side13": np.array([3,4,5]) })
spatialDetectionNetwork.setIouThreshold(0.5)

# Create outputs

monoLeft.out.link(stereo.left)
monoRight.out.link(stereo.right)

colorCam.preview.link(spatialDetectionNetwork.input)
colorCam.preview.link(manip.inputImage)
manip.out.link(segmentation_nn.input)
if(syncNN):
	spatialDetectionNetwork.passthrough.link(xoutRgb.input)
else:
	colorCam.preview.link(xoutRgb.input)

spatialDetectionNetwork.out.link(xoutNN.input)
spatialDetectionNetwork.boundingBoxMapping.link(xoutBoundingBoxDepthMapping.input)

stereo.depth.link(spatialDetectionNetwork.inputDepth)
#spatialDetectionNetwork.passthroughDepth.link(xoutDepth.input)
segmentation_nn.out.link(xoutSegm.input)

'''
"14442C109130A1D000" - 1 or blue
"14442C10C1FAA1D000" - 2 or green
"14442C10C1CF0FD100" - 3 or yellow
"14442C100199A1D000" - 4 or orange
'''
found, device_info = None, None
if cam_id != '':
	found, device_info = dai.Device.getDeviceByMxId(cam_id)

	if not found:
		raise RuntimeError("Device not found!")
else:
	print("No camera ID specified, finding one")
	for device in dai.Device.getAllAvailableDevices():
		print(f"{device.getMxId()} {device.state}")
		cam_id = device.getMxId()
	if cam_id != '':
		print("Using camera ",cam_id)
		found, device_info = dai.Device.getDeviceByMxId(cam_id)
	else:
		raise RuntimeError("No device found!")

# Pipeline defined, now the device is connected to
#with dai.Device(pipeline) as device:
with dai.Device(pipeline, device_info) as device:
	# Start pipeline
	device.startPipeline()

	# Output queues will be used to get the rgb frames and nn data from the outputs defined above
	previewQueue = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
	detectionNNQueue = device.getOutputQueue(name="detections", maxSize=4, blocking=False)
	#xoutBoundingBoxDepthMapping = device.getOutputQueue(name="boundingBoxDepthMapping", maxSize=4, blocking=False)
	#depthQueue = device.getOutputQueue(name="depth", maxSize=4, blocking=False)
	segmentationNNQueue = device.getOutputQueue(name="segmentation", maxSize=4, blocking=False)  
  
	frame = None
	output_colors = None
	detections = []

	startTime = time.monotonic()
	counter = 0
	fps = 0
	layer_info_printed = False
	color = (255, 0, 0)

	while True:
		try:
			inPreview = previewQueue.tryGet()
			inNN = detectionNNQueue.tryGet()
			inSegm = segmentationNNQueue.tryGet()
			#depth = depthQueue.get()
		except RuntimeError as e:
			print("RuntimeError",e)

			device.startPipeline()
			continue
			
		counter+=1
		current_time = time.monotonic()
		if (current_time - startTime) > 1 :
			fps = counter / (current_time - startTime)
			counter = 0
			startTime = current_time
		
		#depthFrame = depth.getFrame()

		if inSegm is not None:
			# print("NN received")
			layers = inSegm.getAllLayers()

			if not layer_info_printed:
				for layer_nr, layer in enumerate(layers):
					print(f"Layer {layer_nr}")
					print(f"Name: {layer.name}")
					print(f"Order: {layer.order}")
					print(f"dataType: {layer.dataType}")
					dims = layer.dims[::-1] # reverse dimensions
					print(f"dims: {dims}")
				layer_info_printed = True

			# get layer1 data
			layer1 = inSegm.getLayerInt32(layers[0].name)
			# reshape to numpy array
			dims = layer.dims[::-1]
			lay1 = np.asarray(layer1, dtype=np.int32).reshape(dims)    
			
			output_colors = decode_deeplabv3p(lay1)


		#depthFrameColor = cv2.normalize(depthFrame, None, 255, 0, cv2.NORM_INF, cv2.CV_8UC1)
		#depthFrameColor = cv2.equalizeHist(depthFrameColor)
		#depthFrameColor = cv2.applyColorMap(depthFrameColor, cv2.COLORMAP_HOT)
		if inNN is not None:
			detections = inNN.detections
			'''if len(detections) != 0:
				boundingBoxMapping = xoutBoundingBoxDepthMapping.get()
				roiDatas = boundingBoxMapping.getConfigData()

				for roiData in roiDatas:
					roi = roiData.roi
					roi = roi.denormalize(depthFrameColor.shape[1], depthFrameColor.shape[0])
					topLeft = roi.topLeft()
					bottomRight = roi.bottomRight()
					xmin = int(topLeft.x)
					ymin = int(topLeft.y)
					xmax = int(bottomRight.x)
					ymax = int(bottomRight.y)

					cv2.rectangle(depthFrameColor, (xmin, ymin), (xmax, ymax), color, cv2.FONT_HERSHEY_SCRIPT_SIMPLEX)
			'''

		# if the frame is available, draw bounding boxes on it and show the frame
		if inPreview is not None:
			frame = inPreview.getCvFrame()
			height = frame.shape[0]
			width  = frame.shape[1]
			for detection in detections:
				# denormalize bounding box
				x1 = int(detection.xmin * width)
				x2 = int(detection.xmax * width)
				y1 = int(detection.ymin * height)
				y2 = int(detection.ymax * height)
				try:
					label = labelMap[detection.label]
				except:
					label = detection.label
				cv2.putText(frame, str(label), (x1 + 10, y1 + 20), cv2.FONT_HERSHEY_TRIPLEX, 1.0, color)
				cv2.putText(frame, "{:.2f}".format(detection.confidence*100), (x1 + 10, y1 + 35), cv2.FONT_HERSHEY_TRIPLEX, 0.5, color)
				cv2.putText(frame, f"X: {int(detection.spatialCoordinates.x)} mm", (x1 + 10, y1 + 50), cv2.FONT_HERSHEY_TRIPLEX, 0.5, color)
				cv2.putText(frame, f"Y: {int(detection.spatialCoordinates.y)} mm", (x1 + 10, y1 + 65), cv2.FONT_HERSHEY_TRIPLEX, 0.5, color)
				cv2.putText(frame, f"Z: {int(detection.spatialCoordinates.z)} mm", (x1 + 10, y1 + 80), cv2.FONT_HERSHEY_TRIPLEX, 0.5, color)

				cv2.rectangle(frame, (x1, y1), (x2, y2), color, cv2.FONT_HERSHEY_SIMPLEX)

			cv2.putText(frame, "NN fps: {:.2f}".format(fps), (2, frame.shape[0] - 4), cv2.FONT_HERSHEY_TRIPLEX, 0.4, color)
			if output_colors is not None:
				frame = show_deeplabv3p(output_colors, frame)
			#cv2.imshow("depth", depthFrameColor)
			cv2.imshow("rgb", frame)

		if cv2.waitKey(1) == ord('q'):
			break
