#include <chrono>
#include <cstdio>
#include <iostream>

#include "utility.hpp"

// Inludes common necessary includes for development using depthai library
#include "depthai/depthai.hpp"


static bool syncNN = true;
int nn_shape = 256

dai::Pipeline createNNPipeline(std::string nnPath, int nn_shape) {

    

    dai::Pipeline p;
    p->setOpenVINOVersion(OpenVINO::VERSION_2021_2);

    // Define a neural network that will make predictions based on the source frames
    // detection_nn = pipeline.createNeuralNetwork()
    auto detection_nn = p.create<dai::node::NeuralNetwork>();
    //detection_nn.setBlobPath(nn_path)
    detection_nn->setBlobPath(nnPath);

    // detection_nn.setNumPoolFrames(4)
    etection_nn->setNumPoolFrames(4);
    //detection_nn.input.setBlocking(False)
    detection_nn->input->setBlocking(false);
    //detection_nn.setNumInferenceThreads(2)
    detection_nn->setNumInferenceThreads(2);

    //cam = pipeline.createColorCamera()
    auto cam = p.create<dai::node::ColorCamera>();
    //cam.setPreviewSize(nn_shape,nn_shape)
    cam->setPreviewSize(nn_shape,nn_shape);
    //cam.setInterleaved(False)
    colorCam->setInterleaved(false);
    //cam.preview.link(detection_nn.input)
    cam->preview.link(detection_nn->input);

    //cam.setFps(40)
    cam->setFps(40);

    // Create outputs
    //xout_rgb = pipeline.createXLinkOut()
    auto xout_rgb = p.create<dai::node::XLinkOut>();
    //xout_rgb.setStreamName("nn_input")
    xout_rgb->setStreamName("nn_input");
    //xout_rgb.input.setBlocking(False)
    xout_rgb.input->setBlocking(false);

    //detection_nn.passthrough.link(xout_rgb.input)
    detection_nn->passthrough->link(xout_rgb->input);

    //xout_nn = pipeline.createXLinkOut()
    xout_nn = p.create<dai::node::XLinkOut>();
    //xout_nn.setStreamName("nn")
    xout_nn->setStreamName("nn");
    //xout_nn.input.setBlocking(False)
    xout_nn->input->setBlocking(false);

    //detection_nn.out.link(xout_nn.input)
    detection_nn->out->link(xout_nn->input);


    /*
    // create nodes
    auto colorCam = p.create<dai::node::ColorCamera>();
    auto spatialDetectionNetwork = p.create<dai::node::YoloSpatialDetectionNetwork>();
    auto monoLeft = p.create<dai::node::MonoCamera>();
    auto monoRight = p.create<dai::node::MonoCamera>();
    auto stereo = p.create<dai::node::StereoDepth>();

    // create xlink connections
    auto xoutRgb = p.create<dai::node::XLinkOut>();
    auto xoutNN = p.create<dai::node::XLinkOut>();
    auto xoutBoundingBoxDepthMapping = p.create<dai::node::XLinkOut>();
    auto xoutDepth = p.create<dai::node::XLinkOut>();

    xoutRgb->setStreamName("preview");
    xoutNN->setStreamName("detections");
    xoutBoundingBoxDepthMapping->setStreamName("boundingBoxDepthMapping");
    xoutDepth->setStreamName("depth");

    colorCam->setPreviewSize(416, 416);
    colorCam->setResolution(dai::ColorCameraProperties::SensorResolution::THE_1080_P);
    colorCam->setInterleaved(false);
    colorCam->setColorOrder(dai::ColorCameraProperties::ColorOrder::BGR);

    monoLeft->setResolution(dai::MonoCameraProperties::SensorResolution::THE_400_P);
    monoLeft->setBoardSocket(dai::CameraBoardSocket::LEFT);
    monoRight->setResolution(dai::MonoCameraProperties::SensorResolution::THE_400_P);
    monoRight->setBoardSocket(dai::CameraBoardSocket::RIGHT);

    /// setting node configs
    stereo->setOutputDepth(true);
    stereo->setConfidenceThreshold(255);

    spatialDetectionNetwork->setBlobPath(nnPath);
    spatialDetectionNetwork->setConfidenceThreshold(0.5f);
    spatialDetectionNetwork->input.setBlocking(false);
    spatialDetectionNetwork->setBoundingBoxScaleFactor(0.5);
    spatialDetectionNetwork->setDepthLowerThreshold(100);
    spatialDetectionNetwork->setDepthUpperThreshold(5000);

    // yolo specific parameters
    spatialDetectionNetwork->setNumClasses(80);
    spatialDetectionNetwork->setCoordinateSize(4);
    spatialDetectionNetwork->setAnchors({10, 14, 23, 27, 37, 58, 81, 82, 135, 169, 344, 319});
    spatialDetectionNetwork->setAnchorMasks({{"side13", {3, 4, 5}}, {"side26", {1, 2, 3}}});
    spatialDetectionNetwork->setIouThreshold(0.5f);

    // Link plugins CAM -> STEREO -> XLINK
    monoLeft->out.link(stereo->left);
    monoRight->out.link(stereo->right);

    // Link plugins CAM -> NN -> XLINK
    colorCam->preview.link(spatialDetectionNetwork->input);
    if(syncNN)
        spatialDetectionNetwork->passthrough.link(xoutRgb->input);
    else
        colorCam->preview.link(xoutRgb->input);

    spatialDetectionNetwork->out.link(xoutNN->input);
    spatialDetectionNetwork->boundingBoxMapping.link(xoutBoundingBoxDepthMapping->input);

    stereo->depth.link(spatialDetectionNetwork->inputDepth);
    spatialDetectionNetwork->passthroughDepth.link(xoutDepth->input);
    */
    return p;
}

int main(int argc, char** argv) {
    using namespace std;
    using namespace std::chrono;
    std::string nnPath;

    // If path to blob specified, use that
    if(argc > 1) {
        nnPath = std::string(argv[1]);
    }else{
        return -1
    }

    // Print which blob we are using
    printf("Using blob at path: %s\n", nnPath.c_str());

    // Create pipeline
    dai::Pipeline p = createNNPipeline(nnPath);

    // Connect to device with above created pipeline
    dai::Device d(p);
    // Start the pipeline
    d.startPipeline();

    auto preview = d.getOutputQueue("nn_input", 4, false);
    auto detections = d.getOutputQueue("nn", 4, false);
    
    auto startTime = steady_clock::now();
    int counter = 0;
    float fps = 0;
    auto color = cv::Scalar(255, 255, 255);

    while(1) {
        auto imgFrame = preview->get<dai::ImgFrame>();
        auto det = detections->get<dai::SpatialImgDetections>();
        
        counter++;
        auto currentTime = steady_clock::now();
        auto elapsed = duration_cast<duration<float>>(currentTime - startTime);
        if(elapsed > seconds(1)) {
            fps = counter / elapsed.count();
            counter = 0;
            startTime = currentTime;
        }

        cv::Mat frame = imgFrame->getCvFrame();

        for(const auto& d : dets) {
            int x1 = d.xmin * frame.cols;
            int y1 = d.ymin * frame.rows;
            int x2 = d.xmax * frame.cols;
            int y2 = d.ymax * frame.rows;

        }

        std::stringstream fpsStr;
        fpsStr << std::fixed << std::setprecision(2) << fps;
        cv::putText(frame, fpsStr.str(), cv::Point(2, imgFrame->getHeight() - 4), cv::FONT_HERSHEY_TRIPLEX, 0.4, color);

        cv::imshow("preview", frame);
        int key = cv::waitKey(1);
        if(key == 'q') {
            return 0;
        }
    }

    return 0;
}